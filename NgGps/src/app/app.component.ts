import { Component, OnInit } from '@angular/core';
import { HeaderService } from './services/header/header.service';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private headerService: HeaderService, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry
    .addSvgIcon('device', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/device.svg'))
    .addSvgIcon('excellent', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/excellent.svg'))
    .addSvgIcon('flag', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/flag.svg'))
    .addSvgIcon('not-usable', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/not-usable.svg'))
    .addSvgIcon('out-of-range', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/out-of-range.svg'))
    .addSvgIcon('poor', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/poor.svg'))
    .addSvgIcon('power', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/power.svg'))
    .addSvgIcon('train', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/train.svg'))
    .addSvgIcon('very-good', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/very-good.svg'))
    .addSvgIcon('good', sanitizer.bypassSecurityTrustResourceUrl('/assets/images/good.svg'));
  }

  ngOnInit() {
    
  }
}
