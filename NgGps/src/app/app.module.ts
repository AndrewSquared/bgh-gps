import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DateSelectComponent } from './header/date-select/date-select.component';
import { SubdivisionComponent } from './header/subdivision/subdivision.component';
import { DaySelectComponent } from './header/day-select/day-select.component';
import { DaySelectService } from "./services/day-select/day-select.service";
import { SubdivisionService } from './services/subdivision/subdivision.service';
import { SearchInputComponent } from './header/search-input/search-input.component';
import { SearchInputService } from "./services/search-input/search-input.service";
import { HeaderService } from './services/header/header.service';
import { MainContentComponent } from './main-content/main-content.component';
import { SignalQualityComponent } from './header/signal-quality/signal-quality.component';
import { SignalQualityService } from './services/signal-quality/signal-quality.service';
import { SplashPageComponent } from './splash-page/splash-page.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DateSelectComponent,
    SubdivisionComponent,
    DaySelectComponent,
    SearchInputComponent,
    MainContentComponent,
    SignalQualityComponent,
    SplashPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    DaySelectService,
    SubdivisionService,
    SearchInputService,
    HeaderService,
    SignalQualityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
