import {Component, OnInit, Output} from '@angular/core';
import { DaySelectService } from "../../services/day-select/day-select.service";

@Component({
  selector: 'app-date-select',
  templateUrl: './date-select.component.html',
  styleUrls: ['./date-select.component.scss']
})
export class DateSelectComponent implements OnInit {


  constructor(private daySelectService: DaySelectService) {
  }

  ngOnInit() {
    this.daySelectService.setShowVar(false);
  }

  toggleDays() {
    this.daySelectService.showVar = !this.daySelectService.showVar;
  }
}
