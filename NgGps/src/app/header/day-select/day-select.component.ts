import { Component, Input, OnInit } from '@angular/core';
import { DaySelectService } from "../../services/day-select/day-select.service";

@Component({
  selector: 'app-day-select',
  templateUrl: './day-select.component.html',
  styleUrls: ['./day-select.component.scss']
})
export class DaySelectComponent implements OnInit {

  constructor(public daySelectService: DaySelectService) { }

  ngOnInit() { }

  selectDay(num: string) {
    this.daySelectService.setDayNum(num);
  }
}
