import { Component, OnInit, OnChanges } from '@angular/core';

import { HeaderService } from '../services/header/header.service';
import { HeaderInfo } from '../shared/header-info.model';
import { Train } from '../shared/train.model';
import { SubdivisionService } from '../services/subdivision/subdivision.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerInfo: HeaderInfo;

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.headerInfo = this.headerService.headerInfo;
    console.log(this.headerInfo);

    if (this.isSearchBySubdivisionAndSignals(this.headerInfo)) {
      console.log('Calling isSearchBySubdivisionAndSignals');
      this.headerService
        .getTrainsBySubdivisionAndSignals(this.headerInfo.subdivision,
          this.getFormattedDate(this.headerInfo.dayOfTheMonth),
          this.getFormattedSignals(this.headerInfo.signalList));
    } else if (this.isSearchBySubdivisionAndTrainAndSignals(this.headerInfo)) {
      console.log('Calling isSearchBySubdivisionAndTrainAndSignals');
      this.headerService
        .getTrainsBySubdivisionAndTrainAndSignals(this.headerInfo.subdivision,
          this.getFormattedDate(this.headerInfo.dayOfTheMonth),
          this.headerInfo.train,
          this.getFormattedSignals(this.headerInfo.signalList));
    } else if (this.isSearchBySubdivisionAndLocomotiveAndSignals(this.headerInfo)) {
      console.log('Calling isSearchBySubdivisionAndLocomotiveAndSignals');
      this.headerService
        .getTrainsBySubdivisionAndLocomotiveAndSignals(this.headerInfo.subdivision,
          this.getFormattedDate(this.headerInfo.dayOfTheMonth),
          this.headerInfo.locomotive,
          this.getFormattedSignals(this.headerInfo.signalList));
    } else if (this.isSearchBySubdivision(this.headerInfo)) {
      console.log('Calling isSearchBySubdivision');
      this.headerService
        .getTrainsBySubdivision(this.headerInfo.subdivision,
          this.getFormattedDate(this.headerInfo.dayOfTheMonth));
    } else if (this.isSearchBySubdivisionAndLocomotive(this.headerInfo)) {
      console.log('Calling isSearchBySubdivisionAndLocomotive');
      this.headerService
        .getTrainsBySubdivisionAndLocomotive(this.headerInfo.subdivision,
          this.getFormattedDate(this.headerInfo.dayOfTheMonth),
          this.headerInfo.locomotive);
    } else if (this.isSearchBySubdivisionAndTrain(this.headerInfo)) {
      console.log('Calling isSearchBySubdivisionAndTrain');
      this.headerService
        .getTrainsBySubdivisionAndTrain(this.headerInfo.subdivision,
          this.getFormattedDate(this.headerInfo.dayOfTheMonth),
          this.headerInfo.train);
    }
  }

  private isSearchBySubdivision(headerInfo: HeaderInfo) {
    return !headerInfo.locomotive && !headerInfo.train;
  }

  private isSearchBySubdivisionAndSignals(headerInfo: HeaderInfo) {
    return !headerInfo.locomotive && !headerInfo.train && !this.isSignalListEmpty(headerInfo.signalList);
  }

  private isSignalListEmpty(signals: string[]) {
    return !signals || signals.length == 0;
  }

  private isSearchBySubdivisionAndTrain(headerInfo: HeaderInfo) {
    return headerInfo.train && !headerInfo.locomotive;
  }

  private isSearchBySubdivisionAndTrainAndSignals(headerInfo: HeaderInfo) {
    return headerInfo.train && !headerInfo.locomotive && !this.isSignalListEmpty(headerInfo.signalList);
  }

  private isSearchBySubdivisionAndLocomotive(headerInfo: HeaderInfo) {
    return !headerInfo.train && headerInfo.locomotive;
  }

  private isSearchBySubdivisionAndLocomotiveAndSignals(headerInfo: HeaderInfo) {
    return !headerInfo.train && headerInfo.locomotive && !this.isSignalListEmpty(headerInfo.signalList);
  }

  private getFormattedDate(date: string) {
    return '01' + date.padStart(2, '0') + '2018';
  }

  private getFormattedSignals(signals: string[]) {
    var formattedSignals = signals.join().toUpperCase();
    return signals.join();
  }

}
