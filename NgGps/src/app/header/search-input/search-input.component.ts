import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { SearchInputService } from "../../services/search-input/search-input.service";

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {

  userInput: string;

  constructor(private searchInputService: SearchInputService) { }

  ngOnInit() {
    this.userInput = "";
  }

  onUserInput(event) {
    this.searchInputService.setUserInput(this.userInput);
  }

}
