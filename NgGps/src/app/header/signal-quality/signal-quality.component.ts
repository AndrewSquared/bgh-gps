import { Component, OnInit, OnChanges, DoCheck } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SignalQualityService } from "../../services/signal-quality/signal-quality.service";

@Component({
  selector: 'app-signal-quality',
  templateUrl: './signal-quality.component.html',
  styleUrls: ['./signal-quality.component.scss']
})
export class SignalQualityComponent implements OnInit {
  signals = new FormControl();
  signalList = [
    { value: 'e', text: 'Excellent', icon: 'excellent' }, 
    { value: 'v', text: 'Very Good', icon: 'very-good' }, 
    {value: 'g', text: 'Good', icon: 'good'}, 
    {value: 'p', text: 'Poor', icon: 'poor'}, 
    { value: 'n', text: 'Not Usable', icon: 'not-usable'}, 
    {value:'x', text: 'Out of Range', icon: 'out-of-range'}
  ];

  constructor(private signalQualityService: SignalQualityService) { }

  ngOnInit() { 
    this.signals.valueChanges.subscribe(value => {
      this.signalQualityService.setQualityList(value);
    });
   }
}
