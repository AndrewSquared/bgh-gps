import { Component, OnInit, OnChanges } from '@angular/core';
import { HeaderService } from '../../services/header/header.service';

@Component({
  selector: 'app-subdivision',
  templateUrl: './subdivision.component.html',
  styleUrls: ['./subdivision.component.scss']
})
export class SubdivisionComponent implements OnInit {
  subdivisions = [
    { value: null, viewValue: 'None' },
    { value: 'fn', viewValue: 'FN' },
    { value: 'fz', viewValue: 'FZ' },
    { value: 'he', viewValue: 'HE' },
    { value: 'to', viewValue: 'TO' },
    { value: 'na', viewValue: 'NA' },
    { value: 'sn', viewValue: 'SN' },
    { value: 'ri', viewValue: 'RI' },
    { value: 'mh', viewValue: 'MH' },
    { value: 'cp', viewValue: 'CP' },
    { value: 'ct', viewValue: 'CT' },
    { value: 'pi', viewValue: 'PI' },
    { value: 'ln', viewValue: 'LN' },
    { value: 'lc', viewValue: 'LC' },
    { value: 'ml', viewValue: 'ML' },
    { value: 'se', viewValue: 'SE' },
    { value: 'ch', viewValue: 'CH' },
    { value: 'sk', viewValue: 'SK' },
    { value: 'nd', viewValue: 'ND' },
    { value: 'mt', viewValue: 'MT' },
    { value: 'mm', viewValue: 'MM' },
    { value: 'it', viewValue: 'IT' },
    { value: 'b1', viewValue: 'B1' },
    { value: 'wa', viewValue: 'WA' },
    { value: 'my', viewValue: 'MY' },
    { value: 'ag', viewValue: 'AG' },
    { value: 'bk', viewValue: 'BK' },
    { value: 'ce', viewValue: 'CE' },
    { value: 'js', viewValue: 'JS' },
    { value: 'cu', viewValue: 'CU' },
    { value: 'pa', viewValue: 'PA' },
    { value: 'aa', viewValue: 'AA' },
    { value: 'jr', viewValue: 'JR' },
    { value: 'cs', viewValue: 'CS' },
    { value: 'or', viewValue: 'OR' },
    { value: 'nh', viewValue: 'NH' },
    { value: 'bl', viewValue: 'BL' },
    { value: 'bu', viewValue: 'BU' },
    { value: 'ew', viewValue: 'EW' },
    { value: 'jt', viewValue: 'JT' },
    { value: 'no', viewValue: 'NO' },
    { value: 'kw', viewValue: 'KW' },
    { value: 'ak', viewValue: 'AK' },
    { value: 'tt', viewValue: 'TT' },
    { value: 'ss', viewValue: 'SS' },
    { value: 'ip', viewValue: 'IP' },
    { value: 'tn', viewValue: 'TN' },
    { value: 'cq', viewValue: 'CQ' },
    { value: 'rr', viewValue: 'RR' },
    { value: 'bz', viewValue: 'BZ' },
    { value: 's0', viewValue: 'S0' },
    { value: 'do', viewValue: 'DO' },
    { value: 'nr', viewValue: 'NR' },
    { value: 'lt', viewValue: 'LT' },
    { value: 'me', viewValue: 'ME' },
    { value: 'm0', viewValue: 'M0' },
    { value: 'nm', viewValue: 'NM' },
    { value: 'st', viewValue: 'ST' },
    { value: 'th', viewValue: 'TH' },
    { value: 'sb', viewValue: 'SB' },
    { value: 'ht', viewValue: 'HT' },
    { value: 'bs', viewValue: 'BS' },
    { value: 'aw', viewValue: 'AW' },
    { value: 'mn', viewValue: 'MN' },
    { value: 'm5', viewValue: 'M5' },
    { value: 'c3', viewValue: 'C3' },
    { value: 'c2', viewValue: 'C2' },
    { value: 'ev', viewValue: 'EV' },
    { value: 'hl', viewValue: 'HL' },
    { value: 'ps', viewValue: 'PS' },
    { value: 'ws', viewValue: 'WS' },
    { value: 'rv', viewValue: 'RV' },
    { value: 's7', viewValue: 'S7' },
    { value: 'pj', viewValue: 'PJ' },
    { value: 'b0', viewValue: 'B0' },
    { value: 'p5', viewValue: 'P5' },
    { value: 'sh', viewValue: 'SH' },
    { value: 'wt', viewValue: 'WT' },
    { value: 'vn', viewValue: 'VN' },
    { value: 'ze', viewValue: 'ZE' },
    { value: 'au', viewValue: 'AU' },
    { value: 'kp', viewValue: 'KP' },
    { value: 'c9', viewValue: 'C9' },
    { value: 'mw', viewValue: 'MW' },
    { value: 'br', viewValue: 'BR' },
    { value: 'kd', viewValue: 'KD' },
    { value: 'mp', viewValue: 'MP' },
    { value: 'av', viewValue: 'AV' },
    { value: 'cc', viewValue: 'CC' },
    { value: 'z1', viewValue: 'Z1' },
    { value: 'ms', viewValue: 'MS' },
    { value: 'tl', viewValue: 'TL' },
    { value: 'wm', viewValue: 'WM' },
    { value: 'rc', viewValue: 'RC' },
    { value: 'wq', viewValue: 'WQ' },
    { value: 'po', viewValue: 'PO' },
    { value: 'td', viewValue: 'TD' },
    { value: 'ab', viewValue: 'AB' },
    { value: 'lr', viewValue: 'LR' },
    { value: 'an', viewValue: 'AN' }
  ]
  selected: string;

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.subdivisions.sort((a, b) => {
      if (!a.value) {
        return -1;
      }

      return a.value.localeCompare(b.value);
    })
  }

  onSubdivisionSelect(event) {
    this.headerService.setSubdivision(this.selected);
  }

}