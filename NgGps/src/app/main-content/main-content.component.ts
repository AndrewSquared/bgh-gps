import { Component, OnInit } from '@angular/core';

import { Train } from '../shared/train.model';
import { HeaderInfo } from '../shared/header-info.model';
import { HeaderService } from '../services/header/header.service';
import { SubdivisionService } from '../services/subdivision/subdivision.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {
  trains: Train[] = [];

  displayedColumns = ['signalQuality', 'signalStrength', 'locomotiveId', 'trainId', 'deviceId', 'lat', 'lon', 'speed', 'timestamp'];

  poorCount: number;
  oorCount: number;
  noneCount: number;
  goodCount: number;
  veryGoodCount: number;
  excellentCount: number;
  returnLength;

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
    this.headerService.trainsChangedEvent
      .subscribe(trainResponse => {
        this.trains = trainResponse.trains;

        var length = this.trains.length;
        this.poorCount = 0;
        this.oorCount = 0;;
        this.noneCount = 0;
        this.goodCount = 0;;
        this.veryGoodCount = 0;
        this.excellentCount = 0;

        var quality;
        this.returnLength = length;
        for (var i = 0; i < length; i++) {

          quality = this.trains[i]['signalQuality']
          if (quality == 'P') {
            this.poorCount++;
          } else if (quality == 'N') {
            this.noneCount++;
          } else if (quality == 'X') {
            this.oorCount++;
          } else if (quality == 'G') {
            this.goodCount++;
          } else if (quality == 'V') {
            this.veryGoodCount++;
          } else if (quality == 'E') {
            this.excellentCount++;
          }


        }

        console.log(this.trains);
      });
  }

}
