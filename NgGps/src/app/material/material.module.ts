import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatFormFieldModule, MatSelectModule, MatInputModule, MatButtonModule, MatCardModule, MatTableModule, MatTable,
  MatButtonToggleModule, MatPaginatorModule, MatIconModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatIconModule
  ],
  exports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatIconModule
  ],
  declarations: []
})
export class MaterialModule { }
