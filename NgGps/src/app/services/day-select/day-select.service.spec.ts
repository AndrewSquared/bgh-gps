import { TestBed, inject } from '@angular/core/testing';

import { DaySelectService } from './day-select.service';

describe('DaySelectService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DaySelectService]
    });
  });

  it('should be created', inject([DaySelectService], (service: DaySelectService) => {
    expect(service).toBeTruthy();
  }));
});
