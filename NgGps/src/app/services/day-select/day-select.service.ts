import { Injectable } from '@angular/core';

import { HeaderService } from '../header/header.service';

@Injectable()
export class DaySelectService {

  showVar: boolean;
  dayNum: string;

  constructor(private headerService: HeaderService) { }

  setShowVar(showVar: boolean) {
    this.showVar = showVar;
  }

  setDayNum(dayNum: string) {
    this.dayNum = dayNum;
    this.headerService.setDayOfTheMonth(dayNum);
  }
}
