import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HeaderInfo } from '../../shared/header-info.model';
import { Train } from '../../shared/train.model';
import { SubdivisionService } from '../subdivision/subdivision.service';
import { SignalQualityService } from '../signal-quality/signal-quality.service';
import { TrainResponse } from '../../shared/train-response.model';

@Injectable()
export class HeaderService {

  showSplash: boolean = true;
  headerInfo: HeaderInfo = new HeaderInfo();
  trainsChangedEvent: EventEmitter<TrainResponse> = new EventEmitter();

  constructor(private subdivisionService: SubdivisionService) { }

  setSubdivision(subdivision: string) {
    this.headerInfo.setSubdivision(subdivision);
  }

  setDayOfTheMonth(dayOfTheMonth: string) {
    this.headerInfo.setDayOfTheMonth(dayOfTheMonth);
  }

  setLocomotive(locomotive: string) {
    this.headerInfo.setLocomotive(locomotive);
  }

  setTrain(train: string) {
    this.headerInfo.setTrain(train);
  }

  setSignalList(sList: string[]) {
    this.headerInfo.setSignalList(sList);
  }

  setShowSplashPage(showS: boolean) {
    this.showSplash = showS;
  }

  getTrainsBySubdivision(division: string, date: string) {
    this.subdivisionService.getTrainsBySubdivision(division, date)
      .subscribe(trainResponse => {
        this.trainsChangedEvent.emit(trainResponse);
      })
  }

  getTrainsBySubdivisionAndSignals(division: string, date: string, signals: string) {
    this.subdivisionService.getTrainsBySubdivisionAndSignals(division, date, signals)
      .subscribe(trainResponse => {
        this.trainsChangedEvent.emit(trainResponse);
      })
  }

  getTrainsBySubdivisionAndTrain(division: string, date: string, trainId: string) {
    this.subdivisionService.getTrainsBySubdivisionAndTrain(division, date, trainId)
      .subscribe(trainResponse => {
        this.trainsChangedEvent.emit(trainResponse);
      })
  }

  getTrainsBySubdivisionAndTrainAndSignals(division: string, date: string, trainId: string, signals: string) {
    this.subdivisionService.getTrainsBySubdivisionAndTrainAndSignals(division, date, trainId, signals)
      .subscribe(trainResponse => {
        this.trainsChangedEvent.emit(trainResponse);
      })
  }

  getTrainsBySubdivisionAndLocomotive(division: string, date: string, locoId: string) {
    this.subdivisionService.getTrainsBySubdivisionAndLocomotive(division, date, locoId)
      .subscribe(trainResponse => {
        this.trainsChangedEvent.emit(trainResponse);
      })
  }

  getTrainsBySubdivisionAndLocomotiveAndSignals(division: string, date: string, locoId: string, signals: string) {
    this.subdivisionService.getTrainsBySubdivisionAndLocomotiveAndSignals(division, date, locoId, signals)
      .subscribe(trainResponse => {
        this.trainsChangedEvent.emit(trainResponse);
      })
  }

}
