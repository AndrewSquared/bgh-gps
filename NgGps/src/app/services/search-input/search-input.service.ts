import { Injectable } from '@angular/core';

import { HeaderService } from '../header/header.service';

@Injectable()
export class SearchInputService {

  userInput: string;

  constructor(private headerService: HeaderService) { }

  setUserInput(input: string) {
    this.userInput = input;

    if (this.isLocomotiveId(this.userInput)) {
      this.headerService.setLocomotive(this.userInput);
      this.headerService.setTrain(null);
    } else {
      this.headerService.setTrain(this.userInput);
      this.headerService.setLocomotive(null);
    }
  }

  private isLocomotiveId(userInput: string) {
    return userInput.toUpperCase().startsWith('CSXT');
  }

}
