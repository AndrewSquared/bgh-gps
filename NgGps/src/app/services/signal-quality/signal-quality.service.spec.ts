import { TestBed, inject } from '@angular/core/testing';

import { SignalQualityService } from './signal-quality.service';

describe('SignalQualityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignalQualityService]
    });
  });

  it('should be created', inject([SignalQualityService], (service: SignalQualityService) => {
    expect(service).toBeTruthy();
  }));
});
