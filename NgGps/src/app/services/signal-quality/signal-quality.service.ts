import { Injectable } from '@angular/core';
import { HeaderService } from '../header/header.service';

@Injectable()
export class SignalQualityService {

  signalList: string[];

  constructor(private headerService: HeaderService) { }

  setQualityList(sList: string[]) {
    this.signalList = sList;
    this.headerService.setSignalList(this.signalList);
  }
}
