import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Train } from './../../shared/train.model';
import { TrainResponse } from '../../shared/train-response.model';

const API_URL = "http://localhost:3000/api"

@Injectable()
export class SubdivisionService {

    constructor(private http: HttpClient) { }

    getTrainsBySubdivision(division: string, date: string): Observable<TrainResponse> {
        var url = API_URL + '/division/' + division + '/date/' + date;
        return this.http
            .get(url)
            .map(response => {
                var trainResponse = new TrainResponse();
                const trains = response['docs'];

                trainResponse.trains = trains.map((train) => new Train(train));
                trainResponse.resultCount = +response['numFound'];

                return trainResponse;
            });
    }

    getTrainsBySubdivisionAndSignals(division: string, date: string, signals: string): Observable<TrainResponse> {
        var url = API_URL + '/division/' + division + '/date/' + date + '/signalQualities/' + signals;
        return this.http
            .get(url)
            .map(response => {
                var trainResponse = new TrainResponse();
                const trains = response['docs'];

                trainResponse.trains = trains.map((train) => new Train(train));
                trainResponse.resultCount = +response['numFound'];

                return trainResponse;
            });
    }

    getTrainsBySubdivisionAndTrain(division: string, date: string, trainId: string): Observable<TrainResponse> {
        var url = API_URL + '/division/' + division + '/date/' + date + '/trainId/' + trainId;
        return this.http
            .get(url)
            .map(response => {
                var trainResponse = new TrainResponse();
                const trains = response['docs'];

                trainResponse.trains = trains.map((train) => new Train(train));
                trainResponse.resultCount = +response['numFound'];

                return trainResponse;
            });
    }

    getTrainsBySubdivisionAndTrainAndSignals(division: string, date: string, trainId: string, signals: string): Observable<TrainResponse> {
        var url = API_URL + '/division/' + division + '/date/' + date + '/trainId/' + trainId + '/signalQualities/' + signals;
        return this.http
            .get(url)
            .map(response => {
                var trainResponse = new TrainResponse();
                const trains = response['docs'];

                trainResponse.trains = trains.map((train) => new Train(train));
                trainResponse.resultCount = +response['numFound'];

                return trainResponse;
            });
    }

    getTrainsBySubdivisionAndLocomotive(division: string, date: string, locoId: string): Observable<TrainResponse> {
        var url = API_URL + '/division/' + division + '/date/' + date + '/locoId/' + locoId;
        return this.http
            .get(url)
            .map(response => {
                var trainResponse = new TrainResponse();
                const trains = response['docs'];

                trainResponse.trains = trains.map((train) => new Train(train));
                trainResponse.resultCount = +response['numFound'];

                return trainResponse;
            });
    }

    getTrainsBySubdivisionAndLocomotiveAndSignals(division: string, date: string, locoId: string, signals: string): Observable<TrainResponse> {
        var url = API_URL + '/division/' + division + '/date/' + date + '/locoId/' + locoId + '/signalQualities/' + signals;
        return this.http
            .get(url)
            .map(response => {
                var trainResponse = new TrainResponse();
                const trains = response['docs'];

                trainResponse.trains = trains.map((train) => new Train(train));
                trainResponse.resultCount = +response['numFound'];

                return trainResponse;
            });
    }

}