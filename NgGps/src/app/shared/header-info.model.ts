export class HeaderInfo {
    subdivision: string;
    dayOfTheMonth: string;
    locomotive: string;
    train: string;
    signalList: string[];

    setSubdivision(subdivision: string) {
        this.subdivision = subdivision;
    }

    setDayOfTheMonth(dayOfTheMonth: string) {
        this.dayOfTheMonth = dayOfTheMonth;
    }

    setLocomotive(locomotive: string) {
        this.locomotive = locomotive;
    }

    setTrain(train: string) {
        this.train = train;
    }

    setSignalList(sList: string[]) {
        this.signalList = sList;
    }

}