import { Train } from './train.model';

export class TrainResponse {
    trains: Train[];
    resultCount: number;
}