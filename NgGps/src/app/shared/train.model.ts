export class Train {
    latitude: string;
    longitude: string;
    trainId: string;
    speed: number;
    locoId: string;
    signalStrength: string;
    signalQuality: string;
    deviceId: string;
    subdivision: string;
    _timestamp: Date;

    constructor(serviceResponse) {
        this.latitude = serviceResponse['location.lat'];
        this.longitude = serviceResponse['location.lon'];
        this.trainId = serviceResponse['trainId'];
        this.speed = serviceResponse['speed'];
        this.locoId = serviceResponse['locoId'];
        this.signalStrength = serviceResponse['signalStrength'];
        this.signalQuality = serviceResponse['signalQuality'];
        this.deviceId = serviceResponse['deviceId'];
        this.subdivision = serviceResponse['subdiv'];
        this._timestamp = serviceResponse['_timestamp'];
    }

}