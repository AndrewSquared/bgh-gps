import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../services/header/header.service';

@Component({
  selector: 'app-splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.scss']
})
export class SplashPageComponent implements OnInit {

  constructor(private headerService: HeaderService) { }

  ngOnInit() {
  }

  toggleSplash() {
    this.headerService.setShowSplashPage(false);
  }
}
